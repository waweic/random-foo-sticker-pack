This is probably just for me, but in the unlikely case that you want open a pull
request, you should be more or less fulfilling the following requirements:

*  Your content should be leftist (Like, real left, egalitarian stuff, 
   not some social democratic foobar or worse, also, 
   I won't include obvious false flag stuff)

*  The original owner of the content (if there is one) should not care about the 
   copyright.

*  Further down, I will probably mention some more political requirements

*  The final sticker must have a final size of 500px * 500px

*  The final sticker must have a describing name in snake case with hyphens 
   instead of underscores

*  The final sticker must be submitted in a lossless image format, preferably PNG

*  If you did any modifications to one (or multiple) source file(s) 
   (including resizing), please do also include them in their original file 
   format by prefacing the name of the final sticker with source_ or sourceX_ 
   with X being the number of the source

*  It is highly helpful if you also include the file anyone can use to edit the 
   sticker. This would preferably be done with free software 
   like Gnu(IMP)/Glimpse or Krita with the same file name as the final sticker, 
   or, in case of multiple needed files, a zip/tar[.xz/.gz]/rar archive

*  The sticker should fit into a casual instant messaging conversation

*  It is up to my decision whether a sticker fits in or not. I am free to change
   these requirements as I see fit. I am not obligated to share your content if 
   I don't like it. You are totally free to fork this pack and create your own.


**Basically, it's easy. Make your content fit in.**